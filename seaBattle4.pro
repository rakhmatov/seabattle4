#-------------------------------------------------
#
# Project created by QtCreator 2015-11-28T00:02:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = seaBattle4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp

HEADERS  += mainwindow.h \
    cell.h

FORMS    += mainwindow.ui
