#ifndef CELLSWIDGET_H
#define CELLSWIDGET_H

#include <QFrame>
#include <QWidget>

class cellswidget : public QFrame
{
    Q_OBJECT
public:
    explicit cellswidget(QWidget *parent = 0, , Qt::WindowFlags flags);

private:
    int tik_count;
    QColor target_color;
    QColor source_color;
//    ColorStrategyMode color_update_mode;
    int timer_id;

signals:

public slots:

};

#endif // CELLSWIDGET_H
