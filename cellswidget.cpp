#include "cellswidget.h"

#include <QFrame>

cellswidget::cellswidget(QWidget *parent, Qt::WindowFlags flags) :
    QFrame(parent, flags),
    tik_count(0),
    target_color(102, 102, 102),
    source_color(102, 102, 102),
    color_update_mode(update_mode_smooth),
    timer_id(0)
{
    setFrameStyle(QFrame::Panel | QFrame::Raised);
    setLineWidth(1); // cell border width
    setAutoFillBackground(true);
    set_color(target_color);
}
