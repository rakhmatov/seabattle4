#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    int myBattleX[5], myBattleY[5];
    bool battleMod = false;
    bool getMode();
    void setShipAddress(int x, int y);
    ~MainWindow();

private slots:


    void on_actionQuit_triggered();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
