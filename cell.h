#ifndef CELL_H
#define CELL_H

#include "mainwindow.h"

#include <QWidget>

class cell : public QWidget
{
    Q_OBJECT
public:
    explicit cell(QWidget *parent = 0) :
        QWidget(parent),
        tik_count(0),
        target_color(102, 102, 102),
        source_color(102, 102, 102),
        timer_id(0)
    {
        QPalette p(palette());
        setAutoFillBackground(true);
        p.setColor(QPalette::Background, QColor(100, 100, 100));
        setFixedSize(40,40);
        setPalette(p);
        show();
    }
    MainWindow *m;
    int x, y;

private:
    int tik_count;
    QColor target_color;
    QColor source_color;
    int timer_id;

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent * event);


};

#endif // CELL_H
