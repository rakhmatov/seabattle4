#include "cell.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    QFrame * rectFrame;
//    rectFrame->setAutoFillBackground(true);
//    QPalette p(palette());
//    p.setColor(QPalette::Background, QColor(140, 140, 140));
//    rectFrame->setPalette(p);
//    setPalette(p);

    cell *mCell;
    ui->gridLayout->setSpacing(1);
//    ui->gridLayout->addWidget(mCell);
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
        mCell = new cell(this);
        mCell->x = j;
        mCell->y = i;
        ui->gridLayout->addWidget(mCell,i,j);
        }
    }


    ui->gridLayout_2->setSpacing(1);
//    ui->gridLayout->addWidget(mCell);
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10; j++){
        mCell = new cell(this);
        ui->gridLayout_2->addWidget(mCell,i,j);
        }
    }

//    QList<cell *> list = this->findChildren<cell *>();
//    while(this->findChild<cell *>()){
//        cell *w = this->findChild<cell *>();
//        w = w->nextInFocusChain();
//        if(w == this) break;
//    }
    //    ui->gridLayout->addWidget(rect, 0, 0);
}

bool MainWindow::getMode()
{
    if(this->battleMod == true) qDebug("1");
    else qDebug("0");
    return this->battleMod;
}

void MainWindow::setShipAddress(int x, int y)
{
    myBattleX[0] = x;
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionQuit_triggered()
{
    exit(true);
}

void MainWindow::on_pushButton_clicked()
{
    battleMod = true;
    ui->mode->setText("Battle");
}
